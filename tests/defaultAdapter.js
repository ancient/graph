var cursorAdapterAutoComplete = require('ancient-cursor').adapterAutoComplete;

module.exports = function adapterConstructor() {
  var counter = 1;
  var graphStorage = {};
  
  return function adapterConstructor() {
    
    // (id: String, callback: (error?, result?))
    this.getRef = function(id, callback) {
      callback(undefined, graphStorage[id]);
    };
    
    // (data: any) => id: String
    this.newRef = function(data) {
      return data.id;
    };
    
    // (id: String, callback: (error?, result?: Boolean))
    this.deleteRef = function(adapter, id, callback) {
      if (graphStorage[id]) {
        delete graphStorage[id];
        callback(undefined, true);
      } else {
        callback(undefined, undefined);
      }
    };
    
    // incompleteLinks?: Boolean = false
    this.incompleteLinks = true;
    
    // (link: Link, options: any, callback: (error?, id?: String))
    this.createLink = function(link, options, callback) {
      var newLink = { id: ""+(counter++) };
      if (link.source) newLink.source = link.source;
      if (link.target) newLink.target = link.target;
      graphStorage[newLink.id] = newLink;
      callback(undefined, newLink.id);
    };
    
    // (id: String, uLink: Link, options: any, callback: (error?, count?: Number))
    this.updateLink = function(id, uLink, options, callback) {
      var count = 0;
      if (graphStorage[id]) {
        count++;
        if (uLink.hasOwnProperty('source')) {
          if (uLink.source) graphStorage[id].source = uLink.source;
          else delete graphStorage[id].source;
        }
        if (uLink.hasOwnProperty('target')) {
          if (uLink.target) graphStorage[id].target = uLink.target;
          else delete graphStorage[id].target;
        }
      }
      callback(undefined, count);
    };
    
    // (sLink: Link, uLink: Link, options: any, callback: (error?, count?: Number))
    this.updateLinks = function(sLink, uLink, options, callback) {
      var resultCount = 0;
      for (var id in graphStorage) {
        if (
          ((sLink.hasOwnProperty('source') && sLink.source == graphStorage[id].source) || !sLink.hasOwnProperty('source'))
          &&
          ((sLink.hasOwnProperty('target') && sLink.target == graphStorage[id].target) || !sLink.hasOwnProperty('target'))
        ) {
          this.updateLink(id, uLink, undefined, function(error, count) {
            resultCount+=count;
          });
        }
      }
      callback(undefined, resultCount);
    };
    
    // (sLink: Link, options: any, callback: (error?, count?: Number))
    this.deleteLinks = function(sLink, options, callback) {
      var resultCount = 0;
      for (var id in graphStorage) {
        if (
          ((sLink.hasOwnProperty('source') && sLink.source == graphStorage[id].source) || !sLink.hasOwnProperty('source'))
          &&
          ((sLink.hasOwnProperty('target') && sLink.target == graphStorage[id].target) || !sLink.hasOwnProperty('target'))
        ) {
          delete graphStorage[id];
          resultCount++;
        }
      }
      callback(undefined, resultCount);
    };
    
    // (sLink: Link, options: any) => cursorPointer: any
    this.searchLinks = function(sLink, options) {
      return function(link) {
        return (
          ((sLink.hasOwnProperty('source') && sLink.source == link.source) || !sLink.hasOwnProperty('source'))
          &&
          ((sLink.hasOwnProperty('target') && sLink.target == link.target) || !sLink.hasOwnProperty('target'))
        );
      };
    };
    
    // (pointer: any, callback: (error?, results))
    this.cursorFetch = function(pointer, callback) {
      var results = [];
      for (var id in graphStorage) {
        if (pointer(graphStorage[id])) results.push(graphStorage[id]);
      }
      callback(undefined, results);
    };
    
    // (pointer: any, handler: (error?, result), callback: ())
    this.cursorEach = function(pointer, handler, callback) {
      for (var id in graphStorage) {
        if (pointer(graphStorage[id])) handler(undefined, graphStorage[id]);
      }
      callback(undefined);
    };
    
    cursorAdapterAutoComplete(this);
    
    return this;
  };
};