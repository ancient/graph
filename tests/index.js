var Adapters = require('ancient-adapters').Adapters;
var Refs = require('ancient-refs');
var Graph = require('../index.js');

var testGraphAdapter = require('./testAdapter.js');
var testGraph = require('./testGraph.js');
var testCursorAdapter = require('ancient-cursor/tests/test.js');

var DefaultObjectAdapter = require('./defaultAdapter.js');
var DocumentsFactory = require('ancient-document').Factory;

describe('ancient-graph', function() {
  
  var adapters = new Adapters();
  adapters.add('graph', DefaultObjectAdapter());
  adapters.add('dots', DefaultObjectAdapter());
  adapters.add('cursorTestAdapter', DefaultObjectAdapter());
  
  adapters.addClass('graph', Graph.adaptersAddClass);
  
  var documentsFactory = new DocumentsFactory(adapters);
  var refs = new Refs(documentsFactory);
  
  describe('test demo graphStorage Graph adapter', function() {
    testGraphAdapter(adapters, 'graph', 'dots');
  });
  
  describe('test demo graphStorage Cursor adapter', function() {
    var graph = new Graph(refs, 'cursorTestAdapter');
    var a = graph.create({});
    var b = graph.create({ source: 'cursorTestAdapter/'+a, target: 'cursorTestAdapter/'+a });
    var c = graph.create({ source: 'cursorTestAdapter/'+a, target: 'cursorTestAdapter/'+b });
    testCursorAdapter(graph.search({ source: 'cursorTestAdapter/'+a }), [
      { id: b, source: 'cursorTestAdapter/'+a, target: 'cursorTestAdapter/'+a },
      { id: c, source: 'cursorTestAdapter/'+a, target: 'cursorTestAdapter/'+b }
    ], true, true);
  });
  
  describe('test class Graph demo graphStorage adapter', function() {
    var graph = new Graph(refs, 'graph');
    testGraph(graph, adapters, 'dots');
  });
});