var assert = require('chai').assert;
var Refs = require('ancient-refs');
var async = require('async');
var DocumentsFactory = require('ancient-document').Factory;
var Graph = require('../index.js');

module.exports = function(graph, adapters, dotsAdapterName) {
  var documentsFactory = new DocumentsFactory(adapters);
  var refs = new Refs(documentsFactory);
  
  if (graph.adapter.incompleteLinks) {
    describe('check incomplete links', function() {
      it('should create empty link', function(done) {
        graph.create({}, undefined, function(error, id) {
          refs.get(graph.adapterName+'/'+id, function(error, document) {
            assert.equal(document.data.id, id);
            assert.isNotOk(document.data.source);
            assert.isNotOk(document.data.target);
            done();
          });
        });
      });
      it('should create source-only link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({ source: dotsAdapterName+'/'+dotId }, undefined, function(error, id) {
            refs.get(graph.adapterName+'/'+id, function(error, document) {
              assert.equal(document.data.id, id);
              assert.equal(document.data.source, dotsAdapterName+'/'+dotId);
              assert.isNotOk(document.data.target);
              done();
            });
          });
        });
      });
      it('should create target-only link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({ target: dotsAdapterName+'/'+dotId }, undefined, function(error, id) {
            refs.get(graph.adapterName+'/'+id, function(error, document) {
              assert.equal(document.data.id, id);
              assert.equal(document.data.target, dotsAdapterName+'/'+dotId);
              assert.isNotOk(document.data.source);
              done();
            });
          });
        });
      });
      it('should update complete to incomplete link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({
            source: dotsAdapterName+'/'+dotId,
            target: dotsAdapterName+'/'+dotId
          }, undefined, function(error, id) {
            graph.update(id, {
              source: undefined, target: undefined
            }, undefined, function(error, count) {
              assert.equal(count, 1);
              refs.get(graph.adapterName+'/'+id, function(error, document) {
                assert.equal(document.data.id, id);
                assert.isNotOk(document.data.source);
                assert.isNotOk(document.data.target);
                done();
              });
            });
          });
        });
      });
      it('should update complete to source-only link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({
            source: dotsAdapterName+'/'+dotId,
            target: dotsAdapterName+'/'+dotId
          }, undefined, function(error, id) {
            graph.update(id, {
              target: undefined
            }, undefined, function(error, count) {
              assert.equal(count, 1);
              refs.get(graph.adapterName+'/'+id, function(error, document) {
                assert.equal(document.data.id, id);
                assert.equal(document.data.source, dotsAdapterName+'/'+dotId);
                assert.isNotOk(document.data.target);
                done();
              });
            });
          });
        });
      });
      it('should update complete to target-only link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({
            source: dotsAdapterName+'/'+dotId,
            target: dotsAdapterName+'/'+dotId
          }, undefined, function(error, id) {
            graph.update(id, {
              source: undefined
            }, undefined, function(error, count) {
              assert.equal(count, 1);
              refs.get(graph.adapterName+'/'+id, function(error, document) {
                assert.equal(document.data.id, id);
                assert.isNotOk(document.data.source);
                assert.equal(document.data.target, dotsAdapterName+'/'+dotId);
                done();
              });
            });
          });
        });
      });
      it('should update incomplete to complete link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({}, undefined, function(error, id) {
            graph.update(id, {
              source: dotsAdapterName+'/'+dotId,
              target: dotsAdapterName+'/'+dotId
            }, undefined, function(error, count) {
              assert.equal(count, 1);
              refs.get(graph.adapterName+'/'+id, function(error, document) {
                assert.equal(document.data.id, id);
                assert.equal(document.data.source, dotsAdapterName+'/'+dotId);
                assert.equal(document.data.target, dotsAdapterName+'/'+dotId);
                done();
              });
            });
          });
        });
      });
      it('should update source-only to complete link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({ source: dotsAdapterName+'/'+dotId }, undefined, function(error, id) {
            graph.update(id, {
              target: dotsAdapterName+'/'+dotId
            }, undefined, function(error, count) {
              assert.equal(count, 1);
              refs.get(graph.adapterName+'/'+id, function(error, document) {
                assert.equal(document.data.id, id);
                assert.equal(document.data.source, dotsAdapterName+'/'+dotId);
                assert.equal(document.data.target, dotsAdapterName+'/'+dotId);
                done();
              });
            });
          });
        });
      });
      it('should update target-only to complete link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({ target: dotsAdapterName+'/'+dotId }, undefined, function(error, id) {
            graph.update(id, {
              source: dotsAdapterName+'/'+dotId
            }, undefined, function(error, count) {
              assert.equal(count, 1);
              refs.get(graph.adapterName+'/'+id, function(error, document) {
                assert.equal(document.data.id, id);
                assert.equal(document.data.source, dotsAdapterName+'/'+dotId);
                assert.equal(document.data.target, dotsAdapterName+'/'+dotId);
                done();
              });
            });
          });
        });
      });
      it('should update target-only to source-only link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({ target: dotsAdapterName+'/'+dotId }, undefined, function(error, id) {
            graph.update(id, {
              target: undefined,
              source: dotsAdapterName+'/'+dotId
            }, undefined, function(error, count) {
              assert.equal(count, 1);
              refs.get(graph.adapterName+'/'+id, function(error, document) {
                assert.equal(document.data.id, id);
                assert.equal(document.data.source, dotsAdapterName+'/'+dotId);
                assert.isNotOk(document.data.target);
                done();
              });
            });
          });
        });
      });
      it('should update incomplete to incomplete link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({}, undefined, function(error, id) {
            graph.update(id, {}, undefined, function(error, count) {
              assert.equal(count, 1);
              refs.get(graph.adapterName+'/'+id, function(error, document) {
                assert.equal(document.data.id, id);
                assert.isNotOk(document.data.source);
                assert.isNotOk(document.data.target);
                done();
              });
            });
          });
        });
      });
      it('should update source-only links to target-only links', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          var ids = [];
          async.times(5, function(t, callback) {
            graph.create({ source: dotsAdapterName+'/'+dotId }, undefined, function(error, id) {
              ids.push(id);
              callback();
            });
          }, function() {
            assert.lengthOf(ids, 5);
            graph.update({
              source: dotsAdapterName+'/'+dotId
            }, {
              source: undefined,
              target: dotsAdapterName+'/'+dotId
            }, undefined, function(error, count) {
              assert.ifError(error);
              assert.equal(count, 5);
              async.each(ids, function(id, callback) {
                refs.get(graph.adapterName+'/'+id, function(error, document) {
                  assert.equal(document.data.id, id);
                  assert.isNotOk(document.data.source);
                  assert.equal(document.data.target, dotsAdapterName+'/'+dotId);
                  callback();
                });
              }, function(error) {
                done();
              });
            });
          });
        });
      });
      it('should delete source-only links', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          var ids = [];
          async.times(5, function(t, callback) {
            graph.create({ source: dotsAdapterName+'/'+dotId }, undefined, function(error, id) {
              ids.push(id);
              callback();
            });
          }, function() {
            assert.lengthOf(ids, 5);
            graph.delete({
              source: dotsAdapterName+'/'+dotId
            }, undefined, function(error, count) {
              assert.ifError(error);
              assert.equal(count, 5);
              async.each(ids, function(id, callback) {
                refs.get(graph.adapterName+'/'+id, function(error, document) {
                  assert.isNotOk(document.data);
                  callback();
                });
              }, function(error) {
                done();
              });
            });
          });
        });
      });
      it('should search empty link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({}, undefined, function(error, id) {
            graph.search({
              source: undefined, target: undefined
            }, undefined).first(function(error, document) {
              assert.ifError(error);
              assert.isNotOk(document.data.source);
              assert.isNotOk(document.data.target);
              done();
            });
          });
        });
      });
      it('should search source-only link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({
            source: dotsAdapterName+'/'+dotId
          }, undefined, function(error, id) {
            graph.search({
              source: dotsAdapterName+'/'+dotId
            }, undefined).first(function(error, document) {
              assert.ifError(error);
              assert.equal(document.data.id, id);
              assert.equal(document.data.source, dotsAdapterName+'/'+dotId);
              assert.isNotOk(document.data.target);
              done();
            });
          });
        });
      });
      it('should search target-only link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({
            target: dotsAdapterName+'/'+dotId
          }, undefined, function(error, id) {
            graph.search({
              target: dotsAdapterName+'/'+dotId
            }, undefined).first(function(error, document) {
              assert.ifError(error);
              assert.equal(document.data.id, id);
              assert.isNotOk(document.data.source);
              assert.equal(document.data.target, dotsAdapterName+'/'+dotId);
              done();
            });
          });
        });
      });
      it('should fetch array empty links', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({}, undefined, function(error, id) {
            graph.search({
              source: undefined, target: undefined
            }, undefined).fetch(function(error, results) {
              assert.ifError(error);
              assert.isAbove(results.length, 0);
              done();
            });
          });
        });
      });
      it('should fetch array source-only links', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({
            source: dotsAdapterName+'/'+dotId
          }, undefined, function(error, id) {
            graph.search({
              source: dotsAdapterName+'/'+dotId
            }, undefined).fetch(function(error, results) {
              assert.ifError(error);
              assert.equal(results.length, 1);
              done();
            });
          });
        });
      });
      it('should fetch array target-only links', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({
            target: dotsAdapterName+'/'+dotId
          }, undefined, function(error, id) {
            graph.search({
              target: dotsAdapterName+'/'+dotId
            }, undefined).fetch(function(error, results) {
              assert.ifError(error);
              assert.equal(results.length, 1);
              done();
            });
          });
        });
      });
      it('should fetch array empty links', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({}, undefined, function(error, id) {
            graph.search({
              source: undefined, target: undefined
            }, undefined).fetch(function(error, results) {
              assert.ifError(error);
              assert.isAbove(results.length, 0);
              done();
            });
          });
        });
      });
      it('should fetch each empty links', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({}, undefined, function(error, id) {
            var count = 0;
            graph.search({
              source: undefined, target: undefined
            }, undefined).each(function(error, result) {
              count++;
            }, function() {
              assert.isAbove(count, 0);
              done();
            });
          });
        });
      });
      it('should fetch each source-only links', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({
            source: dotsAdapterName+'/'+dotId
          }, undefined, function(error, id) {
            var count = 0;
            graph.search({
              source: dotsAdapterName+'/'+dotId, target: undefined
            }, undefined).each(function(error, document) {
              count++;
              assert.equal(document.data.source, dotsAdapterName+'/'+dotId);
            }, function() {
              assert.equal(count, 1);
              done();
            });
          });
        });
      });
      it('should fetch each target-only links', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({
            target: dotsAdapterName+'/'+dotId
          }, undefined, function(error, id) {
            var count = 0;
            graph.search({
              source: undefined, target: dotsAdapterName+'/'+dotId
            }, undefined).each(function(error, document) {
              count++;
              assert.equal(document.data.target, dotsAdapterName+'/'+dotId);
            }, function() {
              assert.equal(count, 1);
              done();
            });
          });
        });
      });
      it('should count empty links', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({}, undefined, function(error, id) {
            graph.search({}, undefined).count(function(error, count) {
              assert.isAbove(count, 0);
              done();
            });
          });
        });
      });
      it('should count source-only links', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({
            source: dotsAdapterName+'/'+dotId
          }, undefined, function(error, id) {
            graph.search({
              source: dotsAdapterName+'/'+dotId
            }, undefined).count(function(error, count) {
              assert.equal(count, 1);
              done();
            });
          });
        });
      });
      it('should count target-only links', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          graph.create({
            target: dotsAdapterName+'/'+dotId
          }, undefined, function(error, id) {
            graph.search({
              target: dotsAdapterName+'/'+dotId
            }, undefined).count(function(error, count) {
              assert.equal(count, 1);
              done();
            });
          });
        });
      });
    });
  }

  describe('check complete links', function() {
    it('should create complete link', function(done) {
      adapters.get(dotsAdapterName).createLink({}, undefined, function(error, sourceId) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, targetId) {
          graph.create({
            source: dotsAdapterName+'/'+sourceId,
            target: dotsAdapterName+'/'+targetId
          }, undefined, function(error, id) {
            refs.get(graph.adapterName+'/'+id, function(error, document) {
              assert.equal(document.data.id, id);
              assert.equal(document.data.source, dotsAdapterName+'/'+sourceId);
              assert.equal(document.data.target, dotsAdapterName+'/'+targetId);
              done();
            });
          });
        });
      });
    });
    it('should update complete to complete link', function(done) {
      adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
        graph.create({
          source: dotsAdapterName+'/'+dotId,
          target: dotsAdapterName+'/'+dotId
        }, undefined, function(error, id) {
          graph.update(id, {
            target: graph.adapter.adapterName+'/'+id,
            source: graph.adapter.adapterName+'/'+id
          }, undefined, function(error, count) {
            assert.equal(count, 1);
            refs.get(graph.adapterName+'/'+id, function(error, document) {
              assert.equal(document.data.id, id);
              assert.equal(document.data.source, graph.adapter.adapterName+'/'+id);
              assert.equal(document.data.target, graph.adapter.adapterName+'/'+id);
              done();
            });
          });
        });
      });
    });
    it('should update complete links to complete links', function(done) {
      adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
        var ids = [];
        async.times(5, function(t, callback) {
          graph.create({
            source: dotsAdapterName+'/'+dotId,
            target: dotsAdapterName+'/'+dotId
          }, undefined, function(error, id) {
            ids.push(id);
            callback();
          });
        }, function() {
          assert.lengthOf(ids, 5);
          graph.update({
            source: dotsAdapterName+'/'+dotId,
            target: dotsAdapterName+'/'+dotId
          }, {
            source: graph.adapterName+'/'+ids[0],
            target: graph.adapterName+'/'+ids[0]
          }, undefined, function(error, count) {
            assert.ifError(error);
            assert.equal(count, 5);
            async.each(ids, function(id, callback) {
              refs.get(graph.adapterName+'/'+id, function(error, document) {
                assert.equal(document.data.id, id);
                assert.equal(document.data.source, graph.adapterName+'/'+ids[0]);
                assert.equal(document.data.target, graph.adapterName+'/'+ids[0]);
                callback();
              });
            }, function(error) {
              done();
            });
          });
        });
      });
    });
    it('should delete complete links', function(done) {
      adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
        var ids = [];
        async.times(5, function(t, callback) {
          graph.create({
            source: dotsAdapterName+'/'+dotId,
            target: dotsAdapterName+'/'+dotId
          }, undefined, function(error, id) {
            ids.push(id);
            callback();
          });
        }, function() {
          assert.lengthOf(ids, 5);
          graph.delete({
            source: dotsAdapterName+'/'+dotId,
            target: dotsAdapterName+'/'+dotId
          }, undefined, function(error, count) {
            assert.ifError(error);
            assert.equal(count, 5);
            async.each(ids, function(id, callback) {
              refs.get(graph.adapterName+'/'+id, function(error, document) {
                assert.isNotOk(document.data);
                callback();
              });
            }, function(error) {
              done();
            });
          });
        });
      });
    });
    it('should search complete link', function(done) {
      adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
        graph.create({
          source: dotsAdapterName+'/'+dotId,
          target: dotsAdapterName+'/'+dotId
        }, undefined, function(error, id) {
          graph.search({
            source: dotsAdapterName+'/'+dotId,
            target: dotsAdapterName+'/'+dotId
          }).first(function(error, document) {
            assert.ifError(error);
            assert.equal(document.data.id, id);
            assert.equal(document.data.source, dotsAdapterName+'/'+dotId);
            assert.equal(document.data.target, dotsAdapterName+'/'+dotId);
            done();
          });
        });
      });
    });
    it('should fetch array complete links', function(done) {
      adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
        graph.create({
          target: dotsAdapterName+'/'+dotId,
          source: dotsAdapterName+'/'+dotId
        }, undefined, function(error, id) {
          graph.search({
            target: dotsAdapterName+'/'+dotId,
            source: dotsAdapterName+'/'+dotId
          }, undefined).fetch(function(error, results) {
            assert.ifError(error);
            assert.equal(results.length, 1);
            done();
          });
        });
      });
    });
    it('should fetch each complete links', function(done) {
      adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
        graph.create({
          source: dotsAdapterName+'/'+dotId,
          target: dotsAdapterName+'/'+dotId
        }, undefined, function(error, id) {
          var count = 0;
          graph.search({
            source: dotsAdapterName+'/'+dotId,
            target: dotsAdapterName+'/'+dotId
          }, undefined).each(function(error, document) {
            count++;
            assert.equal(document.data.id, id);
            assert.equal(document.data.source, dotsAdapterName+'/'+dotId);
            assert.equal(document.data.target, dotsAdapterName+'/'+dotId);
          }, function() {
            assert.equal(count, 1);
            done();
          });
        });
      });
    });
    it('should count complete links', function(done) {
      adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
        graph.create({
          target: dotsAdapterName+'/'+dotId,
          source: dotsAdapterName+'/'+dotId
        }, undefined, function(error, id) {
          graph.search({
            target: dotsAdapterName+'/'+dotId,
            source: dotsAdapterName+'/'+dotId
          }, undefined).count(function(error, count) {
            assert.equal(count, 1);
            done();
          });
        });
      });
    });
  });
  
  it('should work as custom class in adapter', function() {
    var graph = adapters.get('graph').asClass('graph');
    assert.instanceOf(graph, Graph);
  });
};