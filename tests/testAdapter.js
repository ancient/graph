var assert = require('chai').assert;
var Refs = require('ancient-refs');
var async = require('async');
var DocumentsFactory = require('ancient-document').Factory;
var Cursor = require('ancient-cursor');

module.exports = function(adapters, graphAdapterName, dotsAdapterName) {
  var documentsFactory = new DocumentsFactory(adapters);
  var refs = new Refs(documentsFactory);
  
  if (adapters.get(graphAdapterName).incompleteLinks) {
    describe('check incomplete links', function() {
      it('should create empty link', function(done) {
        adapters.get(graphAdapterName).createLink({}, undefined, function(error, id) {
          refs.get(graphAdapterName+'/'+id, function(error, document) {
            assert.equal(document.data.id, id);
            assert.isNotOk(document.data.source);
            assert.isNotOk(document.data.target);
            done();
          });
        });
      });
      it('should create source-only link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          adapters.get(graphAdapterName).createLink({ source: dotsAdapterName+'/'+dotId }, undefined, function(error, id) {
            refs.get(graphAdapterName+'/'+id, function(error, document) {
              assert.equal(document.data.id, id);
              assert.equal(document.data.source, dotsAdapterName+'/'+dotId);
              assert.isNotOk(document.data.target);
              done();
            });
          });
        });
      });
      it('should create target-only link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          adapters.get(graphAdapterName).createLink({ target: dotsAdapterName+'/'+dotId }, undefined, function(error, id) {
            refs.get(graphAdapterName+'/'+id, function(error, document) {
              assert.equal(document.data.id, id);
              assert.equal(document.data.target, dotsAdapterName+'/'+dotId);
              assert.isNotOk(document.data.source);
              done();
            });
          });
        });
      });
      it('should update complete to incomplete link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          adapters.get(graphAdapterName).createLink({
            source: dotsAdapterName+'/'+dotId,
            target: dotsAdapterName+'/'+dotId
          }, undefined, function(error, id) {
            adapters.get(graphAdapterName).updateLink(id, {
              source: undefined, target: undefined
            }, undefined, function(error, count) {
              assert.equal(count, 1);
              refs.get(graphAdapterName+'/'+id, function(error, document) {
                assert.equal(document.data.id, id);
                assert.isNotOk(document.data.source);
                assert.isNotOk(document.data.target);
                done();
              });
            });
          });
        });
      });
      it('should update complete to source-only link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          adapters.get(graphAdapterName).createLink({
            source: dotsAdapterName+'/'+dotId,
            target: dotsAdapterName+'/'+dotId
          }, undefined, function(error, id) {
            adapters.get(graphAdapterName).updateLink(id, {
              target: undefined
            }, undefined, function(error, count) {
              assert.equal(count, 1);
              refs.get(graphAdapterName+'/'+id, function(error, document) {
                assert.equal(document.data.id, id);
                assert.equal(document.data.source, dotsAdapterName+'/'+dotId);
                assert.isNotOk(document.data.target);
                done();
              });
            });
          });
        });
      });
      it('should update complete to target-only link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          adapters.get(graphAdapterName).createLink({
            source: dotsAdapterName+'/'+dotId,
            target: dotsAdapterName+'/'+dotId
          }, undefined, function(error, id) {
            adapters.get(graphAdapterName).updateLink(id, {
              source: undefined
            }, undefined, function(error, count) {
              assert.equal(count, 1);
              refs.get(graphAdapterName+'/'+id, function(error, document) {
                assert.equal(document.data.id, id);
                assert.isNotOk(document.data.source);
                assert.equal(document.data.target, dotsAdapterName+'/'+dotId);
                done();
              });
            });
          });
        });
      });
      it('should update incomplete to complete link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          adapters.get(graphAdapterName).createLink({}, undefined, function(error, id) {
            adapters.get(graphAdapterName).updateLink(id, {
              source: dotsAdapterName+'/'+dotId,
              target: dotsAdapterName+'/'+dotId
            }, undefined, function(error, count) {
              assert.equal(count, 1);
              refs.get(graphAdapterName+'/'+id, function(error, document) {
                assert.equal(document.data.id, id);
                assert.equal(document.data.source, dotsAdapterName+'/'+dotId);
                assert.equal(document.data.target, dotsAdapterName+'/'+dotId);
                done();
              });
            });
          });
        });
      });
      it('should update source-only to complete link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          adapters.get(graphAdapterName).createLink({ source: dotsAdapterName+'/'+dotId }, undefined, function(error, id) {
            adapters.get(graphAdapterName).updateLink(id, {
              target: dotsAdapterName+'/'+dotId
            }, undefined, function(error, count) {
              assert.equal(count, 1);
              refs.get(graphAdapterName+'/'+id, function(error, document) {
                assert.equal(document.data.id, id);
                assert.equal(document.data.source, dotsAdapterName+'/'+dotId);
                assert.equal(document.data.target, dotsAdapterName+'/'+dotId);
                done();
              });
            });
          });
        });
      });
      it('should update target-only to complete link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          adapters.get(graphAdapterName).createLink({ target: dotsAdapterName+'/'+dotId }, undefined, function(error, id) {
            adapters.get(graphAdapterName).updateLink(id, {
              source: dotsAdapterName+'/'+dotId
            }, undefined, function(error, count) {
              assert.equal(count, 1);
              refs.get(graphAdapterName+'/'+id, function(error, document) {
                assert.equal(document.data.id, id);
                assert.equal(document.data.source, dotsAdapterName+'/'+dotId);
                assert.equal(document.data.target, dotsAdapterName+'/'+dotId);
                done();
              });
            });
          });
        });
      });
      it('should update target-only to source-only link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          adapters.get(graphAdapterName).createLink({ target: dotsAdapterName+'/'+dotId }, undefined, function(error, id) {
            adapters.get(graphAdapterName).updateLink(id, {
              target: undefined,
              source: dotsAdapterName+'/'+dotId
            }, undefined, function(error, count) {
              assert.equal(count, 1);
              refs.get(graphAdapterName+'/'+id, function(error, document) {
                assert.equal(document.data.id, id);
                assert.equal(document.data.source, dotsAdapterName+'/'+dotId);
                assert.isNotOk(document.data.target);
                done();
              });
            });
          });
        });
      });
      it('should update incomplete to incomplete link', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          adapters.get(graphAdapterName).createLink({}, undefined, function(error, id) {
            adapters.get(graphAdapterName).updateLink(id, {}, undefined, function(error, count) {
              assert.equal(count, 1);
              refs.get(graphAdapterName+'/'+id, function(error, document) {
                assert.equal(document.data.id, id);
                assert.isNotOk(document.data.source);
                assert.isNotOk(document.data.target);
                done();
              });
            });
          });
        });
      });
      it('should update source-only links to target-only links', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          var ids = [];
          async.times(5, function(t, callback) {
            adapters.get(graphAdapterName).createLink({ source: dotsAdapterName+'/'+dotId }, undefined, function(error, id) {
              ids.push(id);
              callback();
            });
          }, function() {
            assert.lengthOf(ids, 5);
            adapters.get(graphAdapterName).updateLinks({
              source: dotsAdapterName+'/'+dotId
            }, {
              source: undefined,
              target: dotsAdapterName+'/'+dotId
            }, undefined, function(error, count) {
              assert.ifError(error);
              assert.equal(count, 5);
              async.each(ids, function(id, callback) {
                refs.get(graphAdapterName+'/'+id, function(error, document) {
                  assert.equal(document.data.id, id);
                  assert.isNotOk(document.data.source);
                  assert.equal(document.data.target, dotsAdapterName+'/'+dotId);
                  callback();
                });
              }, function(error) {
                done();
              });
            });
          });
        });
      });
      it('should delete source-only links', function(done) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
          var ids = [];
          async.times(5, function(t, callback) {
            adapters.get(graphAdapterName).createLink({ source: dotsAdapterName+'/'+dotId }, undefined, function(error, id) {
              ids.push(id);
              callback();
            });
          }, function() {
            assert.lengthOf(ids, 5);
            adapters.get(graphAdapterName).deleteLinks({
              source: dotsAdapterName+'/'+dotId
            }, undefined, function(error, count) {
              assert.ifError(error);
              assert.equal(count, 5);
              async.each(ids, function(id, callback) {
                refs.get(graphAdapterName+'/'+id, function(error, document) {
                  assert.isNotOk(document.data);
                  callback();
                });
              }, function(error) {
                done();
              });
            });
          });
        });
      });
    });
  }

  describe('check complete links', function() {
    it('should create complete link', function(done) {
      adapters.get(dotsAdapterName).createLink({}, undefined, function(error, sourceId) {
        adapters.get(dotsAdapterName).createLink({}, undefined, function(error, targetId) {
          adapters.get(graphAdapterName).createLink({
            source: dotsAdapterName+'/'+sourceId,
            target: dotsAdapterName+'/'+targetId
          }, undefined, function(error, id) {
            refs.get(graphAdapterName+'/'+id, function(error, document) {
              assert.equal(document.data.id, id);
              assert.equal(document.data.source, dotsAdapterName+'/'+sourceId);
              assert.equal(document.data.target, dotsAdapterName+'/'+targetId);
              done();
            });
          });
        });
      });
    });
    it('should update complete to complete link', function(done) {
      adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
        adapters.get(graphAdapterName).createLink({
          source: dotsAdapterName+'/'+dotId,
          target: dotsAdapterName+'/'+dotId
        }, undefined, function(error, id) {
          adapters.get(graphAdapterName).updateLink(id, {
            target: graphAdapterName+'/'+id,
            source: graphAdapterName+'/'+id
          }, undefined, function(error, count) {
            assert.equal(count, 1);
            refs.get(graphAdapterName+'/'+id, function(error, document) {
              assert.equal(document.data.id, id);
              assert.equal(document.data.source, graphAdapterName+'/'+id);
              assert.equal(document.data.target, graphAdapterName+'/'+id);
              done();
            });
          });
        });
      });
    });
    it('should update complete links to complete links', function(done) {
      adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
        var ids = [];
        async.times(5, function(t, callback) {
          adapters.get(graphAdapterName).createLink({
            source: dotsAdapterName+'/'+dotId,
            target: dotsAdapterName+'/'+dotId
          }, undefined, function(error, id) {
            ids.push(id);
            callback();
          });
        }, function() {
          assert.lengthOf(ids, 5);
          adapters.get(graphAdapterName).updateLinks({
            source: dotsAdapterName+'/'+dotId,
            target: dotsAdapterName+'/'+dotId
          }, {
            source: graphAdapterName+'/'+ids[0],
            target: graphAdapterName+'/'+ids[0]
          }, undefined, function(error, count) {
            assert.ifError(error);
            assert.equal(count, 5);
            async.each(ids, function(id, callback) {
              refs.get(graphAdapterName+'/'+id, function(error, document) {
                assert.equal(document.data.id, id);
                assert.equal(document.data.source, graphAdapterName+'/'+ids[0]);
                assert.equal(document.data.target, graphAdapterName+'/'+ids[0]);
                callback();
              });
            }, function(error) {
              done();
            });
          });
        });
      });
    });
    it('should delete complete links', function(done) {
      adapters.get(dotsAdapterName).createLink({}, undefined, function(error, dotId) {
        var ids = [];
        async.times(5, function(t, callback) {
          adapters.get(graphAdapterName).createLink({
            source: dotsAdapterName+'/'+dotId,
            target: dotsAdapterName+'/'+dotId
          }, undefined, function(error, id) {
            ids.push(id);
            callback();
          });
        }, function() {
          assert.lengthOf(ids, 5);
          adapters.get(graphAdapterName).deleteLinks({
            source: dotsAdapterName+'/'+dotId,
            target: dotsAdapterName+'/'+dotId
          }, undefined, function(error, count) {
            assert.ifError(error);
            assert.equal(count, 5);
            async.each(ids, function(id, callback) {
              refs.get(graphAdapterName+'/'+id, function(error, document) {
                assert.isNotOk(document.data);
                callback();
              });
            }, function(error) {
              done();
            });
          });
        });
      });
    });
  });
};