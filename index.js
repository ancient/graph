var Cursor = require('ancient-cursor').Cursor;

/** Adapters requirements **/

// incompleteLinks?: Boolean = false
// createLink(link: Link, options: any, callback: (error?, id?: String))
// updateLink(id: String, uLink: Link, options: any, callback: (error?, count?: Number))
// updateLinks(sLink: Link, uLink: Link, options: any, callback: (error?, count?: Number))
// deleteLinks(sLink: Link, options: any, callback: (error?, count?: Number))
// searchLinks(sLink: Link, options: any) => cursorPointer: any

/** Graph **/
// new (refs: Refs, adapterName: String)
var Graph = function(refs, adapterName) {
  this.refs = refs;
  this.adapters = refs.adapters;
  this.adapterName = adapterName;
  this.adapter = this.adapters.get(this.adapterName);
};

// (link: Link, options?: any, callback?: (error?, id?: String)) => id?: String
Graph.prototype.create = function(link, options, callback) {
  var result;
  this.adapter.createLink(link, options, function(error, id) {
    result = id;
    if (callback) callback(error, result);
  });
  return result;
};

// (query: Id|Link, link: Link, options?: any, callback?: (error?, count?: Number)) => count?: Number
Graph.prototype.update = function(query, link, options, callback) {
  var result;
  if (typeof(query) == 'string') {
    this.adapter.updateLink(query, link, options, function(error, count) {
      result = count;
      if (callback) callback(error, count);
    });
  } else {
    this.adapter.updateLinks(query, link, options, function(error, count) {
      result = count;
      if (callback) callback(error, count);
    });
  }
  return result;
};

// (query: Id|Link, options?: any, callback?: (error?, count?: Number)) => count?: Number
Graph.prototype.delete = function(query, options, callback) {
  var result;
  if (typeof(query) == 'string') {
    this.refs.delete(this.adapterName+'/'+query, function(error, deleted) {
      result = deleted?1:0;
      if (callback) callback(error, result);
    });
  } else {
    this.adapter.deleteLinks(query, options, function(error, count) {
      result = count;
      if (callback) callback(error, count);
    });
  }
  return result;
};

// (query: Id|Link, options?: any) => cursor?: Cursor
Graph.prototype.search = function(query, options) {
  var pointer = this.adapter.searchLinks(query, options);
  var cursor = new Cursor(this.refs, this.adapterName, pointer);
  return cursor;
};

// (adapter: Adapter) => Function
Graph.adaptersAddClass = function(adapter) {
  return new Graph(adapter.adapters.refs, adapter.adapterName);
};

module.exports = Graph;