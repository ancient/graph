# Graph^0.0.8 ^[wiki](https://gitlab.com/ancient/graph/wikis/home)

Adapter based universal interface of graph for any storages and databases.

## Theory

###### `object` adapters

It is a object with user adapted methods of any of databases for some class.

A detailed description of the requirements to the adapter can be found in the [wiki](https://gitlab.com/ancient/graph/wikis/home).

[Adapters repository](https://gitlab.com/ancient/adapters).

###### `object` Link

![Chart](https://gitlab.com/ancient/graph/raw/master/chart-link.png)

Graph link representation standard.

```js
{
  id: "uniqueForCurrentStorageStringId",
  // is not a ref pattern string
  // in ref pattern: "someGraphAdapter/uniqueForCurrentStorageStringId"
  source: "refToAnyAdapter/withAnyData",
  target: "refToAnyAdapter/withAnyData"
}
```

Fields `source` and `target` are not required to be filled if the adapter has `incompleteLinks = true`.

If you want, you can store additional data in link. However, their management is not included in the graph functionality.

###### `Document` support

Returns links wrapped in a [Document](https://gitlab.com/ancient/document) capsule.

```js
document.data; // { id: String, source?: Ref, target?: Ref }
document.Ref() // "someGraphAdapter/uniqueForCurrentStorageStringId"
```

###### `Cursor` support

Search wrapped in a [Cursor](https://gitlab.com/ancient/cursor) class.

## Example

```js
var Refs = require('ancient-refs');
var DocumentsFactory = require('ancient-document').Factory;
var Graph = require('ancient-graph');
var Adapters = require('ancient-adapters').Adapters;
var DefaultObjectAdapter = require('ancient-graph/tests/defaultObjectAdapter');

var adapters = new Adapters();
adapters.add('graph', DefaultObjectAdapter());
adapters.add('dots', DefaultObjectAdapter());

var documentsFactory = new DocumentsFactory(adapters);
var refs = new Refs(documentsFactory);

var graph = new Graph(refs, 'graph');
var dots = new Graph(refs, 'dots');

var dotId = dots.create({});
var linkId = graph.create({ source: dots.adapterName+'/'+dotId, target: dots.adapterName+'/'+dotId });

var cursor = graph.search({ source: dots.adapterName+'/'+dotId, target: dots.adapterName+'/'+dotId });
cursor.first();
// Document { data: { id: linkId, source: dots.adapterName+'/'+dotId, target: dots.adapterName+'/'+dotId } }

graph.update({ target: dots.adapterName+'/'+dotId }, { source: graph.adapterName+'/'+linkId });

graph.search({ source: dots.adapterName+'/'+dotId, target: dots.adapterName+'/'+dotId }).first();
// Document { data: { id: linkId, source: graph.adapterName+'/'+linkId, target: dots.adapterName+'/'+dotId } }

var cursor = graph.search({});
cursor.count();
// 1
cursor.fetch();
// [Document { data: { id: linkId, source: graph.adapterName+'/'+linkId, target: dots.adapterName+'/'+dotId } }]
graph.delete({});
// 1
cursor.count();
// 0

// Use as custom class in adapter

adapters.addClass('graph', Graph.adaptersAddClass);
adapters.get('posts').asClass('graph').search({}).count();
// 0
```

## Versions

### 0.0.8
* Support for adapter custom classes

### 0.0.7
* Add cursorFirst to adapter

### 0.0.6
* Add options support

### 0.0.5
* Remove first argument in all methods adapterName

### 0.0.4
* Support new cursor with slice and first methods.

### 0.0.3
* Support for new Adapters class.

### 0.0.2
* Cursor #2
* Document #5

### 0.0.0
* Basic graph methods